﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SimpleSocketClient
{
    public partial class MainPage : PhoneApplicationPage
    {
        public static string serverIPAddress;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            serverIPAddress = serverIP.Text;
            if (serverIP.Text != null)
            {
                try
                {
                    AsynchronousClient SubClient = new AsynchronousClient(serverIPAddress, 12128);
                    //trayballoon "Hello" "This is a test..." "shell32.dll,22" 15000
                    SubClient.SendData("c:\\command\\nircmd trayballoon \"TakeOver Server\" \"Client is Connected\" \"shell32.dll,22\" 15000");
                    NavigationService.Navigate(new Uri("/BasicControl.xaml", UriKind.Relative));
                }
                catch(Exception evt){
                    ShellToast toast = new ShellToast();
                    toast.Title = "Error!";
                    toast.Content = "There is Some Communication Error. Please setup the connections carefully";
                    toast.Show();
                }
            }
            else {
                ShellToast toast = new ShellToast();
                toast.Title = "Error!";
                toast.Content = "Invalid Server IP";
                toast.Show();
            }
        }

        private void mediaElement1_MediaOpened(object sender, RoutedEventArgs e)
        {

        }

        private void image1_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }
    }
}