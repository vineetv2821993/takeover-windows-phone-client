﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace SimpleSocketClient
{
    public partial class BasicControl : PhoneApplicationPage
    {
        const int SLIDER_VOLUME_MAX = 65535;
        const int SLIDER_BRIGHTNESS_MAX = 100;
        const String COMMAND_TOOL = "c:\\command\\nircmd ";
        int sysIndex,resIndex;

        public class Resolution
        {
            public string Res
            {
                get;
                set;
            }

            public string Bit
            {
                get;
                set;
            }

            public string Type
            {
                get;
                set;
            }
        }

        public class Features
        {
            public string Name
            {
                get;
                set;
            }

            public string Type
            {
                get;
                set;
            }

            public string Available
            {
                get;
                set;
            }
        }
        List<String> sysCommand = new List<String>();
        List<String> resCommand = new List<String>();
        List<String> featCommand = new List<String>();
        public BasicControl()
        {
            sysIndex = resIndex = 0;

            List<String> source = new List<String>();
            source.Add("Screensaver");
            sysCommand.Add("screensaver");
            source.Add("Turn On Display");
            sysCommand.Add("monitor on");
            source.Add("Turn Off Display");
            sysCommand.Add("monitor Off");
            source.Add("Log Off");
            sysCommand.Add("exitwin logoff");
            source.Add("Hibernate");
            sysCommand.Add("hibernate");
            source.Add("Reboot");
            sysCommand.Add("exitwin reboot");
            source.Add("Power Off");
            sysCommand.Add("exitwin poweroff");
            source.Add("Shut Down");
            sysCommand.Add("exitwin shutdown");
            source.Add("Force Log Off");
            sysCommand.Add("exitwin logoff forceifhung");
            source.Add("Force Hibernate");
            sysCommand.Add("hibernate force");
            source.Add("Force Shut Down");
            sysCommand.Add("exitwin shutdownforce");
            source.Add("Force Power Off");
            sysCommand.Add("exittwin poweroff force");

            List<Resolution> resSource = new List<Resolution>();
            resSource.Add(new Resolution() { Res = "800x600", Bit="16", Type="4:3" });
            resCommand.Add("setdisplay 800 600 16");
            resSource.Add(new Resolution() { Res = "800x600", Bit = "32", Type = "4:3" });
            resCommand.Add("setdisplay 800 600 32");
            resSource.Add(new Resolution() { Res = "1024x768", Bit = "16", Type = "4:3" });
            resCommand.Add("setdisplay 1024 768 16");
            resSource.Add(new Resolution() { Res = "1024x768", Bit = "32", Type = "4:3" });
            resCommand.Add("setdisplay 1024 768 32");
            resSource.Add(new Resolution() { Res = "1280x720", Bit = "16", Type = "16:9 HD" });
            resCommand.Add("setdisplay 1280 720 16");
            resSource.Add(new Resolution() { Res = "1280x720", Bit = "32", Type = "16:9 HD" });
            resCommand.Add("setdisplay 1366 768 32");
            resSource.Add(new Resolution() { Res = "1366x768", Bit = "32", Type = "16:9 HD" });
            resCommand.Add("setdisplay 1366 768 32");
            resSource.Add(new Resolution() { Res = "1920x1080", Bit = "32", Type = "16:9 FULL HD" });
            resCommand.Add("setdisplay 1920 1080 32");

            List<Features> featSource = new List<Features>();
            featSource.Add(new Features() { Name = "Coming Soon", Type = "NA", Available = "Available" });
            featCommand.Add("Toast");
            featSource.Add(new Features() { Name = "Air Mouse", Type = "H", Available = "Unavailable" });
            featCommand.Add("Toast");
            featSource.Add(new Features() { Name = "PowerPoint", Type = "S", Available = "Available" });
            featCommand.Add("Toast");
            featSource.Add(new Features() { Name = "Screen Monitoring", Type = "H", Available = "Unavailable" });
            featCommand.Add("Toast");
            InitializeComponent();

            this.ResListPicker.ItemsSource = resSource;
            this.SysListPicker.ItemsSource = source;
            this.featuresListPicker.ItemsSource = featSource;


        }

        private void sliderVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            AsynchronousClient SubClient = new AsynchronousClient(MainPage.serverIPAddress, 12128);
            SubClient.SendData(COMMAND_TOOL + "setsysvolume " + (int)(e.NewValue*0.1*SLIDER_VOLUME_MAX));
        }

        private void sliderBrightness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            AsynchronousClient SubClient = new AsynchronousClient(MainPage.serverIPAddress, 12128);
            SubClient.SendData(COMMAND_TOOL + "setbrightness " + (int)(e.NewValue *0.1* SLIDER_BRIGHTNESS_MAX));
        }

        private void sysListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            sysIndex = SysListPicker.SelectedIndex;
        }

        private void sysDone_Click(object sender, RoutedEventArgs e)
        {
            AsynchronousClient.setCommand(sysCommand.ElementAt(sysIndex));
            AsynchronousClient SubClient = new AsynchronousClient(MainPage.serverIPAddress, 12128);
            SubClient.SendData(COMMAND_TOOL + AsynchronousClient.getCommand());
        }

        private void resListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            resIndex = ResListPicker.SelectedIndex;
        }

        private void resChange_Click(object sender, RoutedEventArgs e)
        {
            AsynchronousClient.setCommand(resCommand.ElementAt(resIndex));
            AsynchronousClient SubClient = new AsynchronousClient(MainPage.serverIPAddress, 12128);
            SubClient.SendData(COMMAND_TOOL + AsynchronousClient.getCommand());

        }

        private void cdromButton_Click(object sender, RoutedEventArgs e)
        {
            AsynchronousClient.setCommand(COMMAND_TOOL + "cdrom open");
            AsynchronousClient SubClient = new AsynchronousClient(MainPage.serverIPAddress, 12128);
            SubClient.SendData(AsynchronousClient.getCommand());

        }

        private void emptyBinButton_Click(object sender, RoutedEventArgs e)
        {
            AsynchronousClient.setCommand(COMMAND_TOOL + "emptybin");
            AsynchronousClient SubClient = new AsynchronousClient(MainPage.serverIPAddress, 12128);
            SubClient.SendData(AsynchronousClient.getCommand());
        }

        private void cdromIButton_Click(object sender, RoutedEventArgs e)
        {
            AsynchronousClient.setCommand(COMMAND_TOOL + "cdrom close");
            AsynchronousClient SubClient = new AsynchronousClient(MainPage.serverIPAddress, 12128);
            SubClient.SendData(AsynchronousClient.getCommand());

        }

        private void speakClipButton_Click(object sender, RoutedEventArgs e)
        {
            AsynchronousClient.setCommand(COMMAND_TOOL + "speak text ~$clipboard$");
            AsynchronousClient SubClient = new AsynchronousClient(MainPage.serverIPAddress, 12128);
            SubClient.SendData(AsynchronousClient.getCommand());
        }

        private void clearClipButton_Click(object sender, RoutedEventArgs e)
        {
            AsynchronousClient.setCommand(COMMAND_TOOL + "clipboard clear");
            AsynchronousClient SubClient = new AsynchronousClient(MainPage.serverIPAddress, 12128);
            SubClient.SendData(AsynchronousClient.getCommand());
        }

    }
}